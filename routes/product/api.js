const router = require('express').Router();
 Product = require('../../models/product');
 const Session = require('../../models/session');
const Methods = require('../../methods/custom');
const jwt = require('jsonwebtoken');
const config = require('../../config/config');
// const express = require('express');
// const secureRoutes = express.Router();
// const secureRoutes = express.Router();
var multer  = require('multer');
var aws = require('aws-sdk');
var fs = require('fs');

/* Multer set storage location*/
var storage = multer.diskStorage({
	
	filename: function (req, file, cb) {
		   var str = file.originalname;
		   str = str.replace(/\s+/g, '-').toLowerCase();
		   global.poster = Date.now() + '_' + str;
		   cb(null, poster);
	 }
   });

   var upload = multer({ storage: storage });
   aws.config.update({ accessKeyId: 'AKIAJFYA2FQR64SJSPGQ', secretAccessKey: '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' });
   aws.config.update({region:'ap-south-1'});

const apiOperation = (req, res, crudMethod, optionsObj)=> {
	const bodyParams = Methods.initializer(req, Product);
	console.log('bodyParams: ', bodyParams);
	crudMethod(bodyParams, optionsObj, (err, product) => {

		console.log('\nproduct details', product);
		res.send(product);
	})
}

router.get('/', (req, res) => {
	res.send('This is the api route for product management');
});

router.get('/getallproducts', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				console.log("SECRET_KEY::"+ config.SECRET_KEY);
				if(err){
					res.status(500).send({status:"failure",message:"Invalid Token"});
				}
				else{
					 const table = `${decode.storeName}_store_data`;
					Product.selectTable(table);
					Product.query('product',(err,products)=>{
						res.send(products);
					})
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

router.post('/getproduct', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const tabletemp = `${decode.storeName}_store_data`;
                    
                    Product.selectTable(tabletemp);
					apiOperation(req, res, Product.getItem);
				}
			})
		}
		else{
			res.send("please send a token");
		}	
});

router.post('/addproduct',upload.single('image'), (req, res) => {
	var token = req.body.token || req.headers['token'];
	console.log("req value: "+JSON.stringify(req.file));
	//console.log("image:"+req.file.originalname);		
	var s3 = new aws.S3();
	var hello=req.file.path;
		if(token){
			console.log(token);
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					
					// console.log("Adding a new item...\n", putParams);
					Session.selectTable(decode.storeName+"_session_data");
					console.log("session_id::::"+decode.sessionId);
					Session.getItem(decode.sessionId,{},(err,session)=>{
						if(err)
						{
							console.log("Error while getting an seesion item::"+ err);
							res.send({status:"error",message:"get session item error!"});
						}
						else
						{

							var product_ids=session.product_ids;
								product_ids.push(req.body.collectionId);
								var putparams={
									session_id:decode.sessionId,
									product_ids:product_ids
								}
							Session.updateItem(putparams,{},(err,updatedSession)=>{

									if(err)
									{
										console.log("error while updating the session with products::"+err);
										res.send({status:"failure",message:"error while updating session with product"});
									}else{

									s3.upload({
										"ACL":"public-read",
									 	"Bucket": "cadenza-images",
									  	"Key": poster,
										"Body": fs.createReadStream(hello),
										 ContentType : "image/jpeg"
			                         	}, function(err, data) {
									if (err) {
										  console.log("Error uploading data: ", err);
									} else {
										console.log("upload success");
										const imageurl="https://s3-ap-southeast-1.amazonaws.com/cadenza-images/"+poster;
										console.log("imageurl:"+imageurl);
										console.log(decode.storeName);
										//const bodyParams = Methods.initializer(req, Product);
										const putParams = {
											"collectionType":"product",
											"collectionId": req.body.collectionId,
											"title": req.body.title,
											"description": req.body.description,
											"price": req.body.price,
											"imageURL":imageurl,
											"retail_price": req.body.retail_price,
											"discount":req.body.discount	
										};

							console.log("putparams:::"+JSON.stringify(putParams));

							Product.createItem(putParams, {
								table: decode.storeName+"_store_data",
								overwrite: false
							}, (err, product) => {
								console.log('\nproduct details', product);
								res.send(product);
							})
						}
					});



									}

							});

						}
					})
					

					
							
						}
						
					});
					
				}
			
		
		else{
			res.send("please send a token");
		}
});

router.put('/updateproduct', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					apiOperation(req, res, Product.updateItem);
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

router.delete('/deleteproduct', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
				
				//apiOperation(req, res, Product.deleteItem);
				const tabletemp = `${decode.storeName}_store_data`
                Product.selectTable(tabletemp)

                const removeCallback = (err, removeData) => {

			console.log("hello this is pallavi");
					var h=removeData.imageURL;
					console.log('\n data is...\n', removeData);
					console.log("deleted image"+h);
				
					console.log("\nDELETED price:"+JSON.parse(removeData.price));
					console.log("\nDELETED price:"+JSON.stringify(removeData.price));
                    console.log('\nThe removed item data is...\n', removeData);
                    res.send(removeData);

		console.log("hello this is pallavi");
					
					console.log('\n data is...\n', removeData);
					console.log("deleted image"+removeData.imageURL);
				
					console.log("\nDELETED price:"+JSON.parse(removeData.price));
					console.log("\nDELETED price:"+JSON.stringify(removeData.price));
                }
          
                // passing the conditional object here {} as second parameter
                Product.deleteItem({
                	collectionType: req.body.collectionType,
                    collectionId: req.body.collectionId
                }, {ReturnValues: 'ALL_OLD'} ,removeCallback);
			}
        })
    }
    else{
        res.send("please send a token");
    }
});

module.exports = router;
