I.PROJECT NAME

Cadenza_product_management

II.PREREQUISITES

Node.js
Node Package Manager(npm)

III.Running Locally

git clone https://gitlab.com/Kuppani/product_management.git
cd product_management
npm install
npm start or node server.js or nodemon start server.js

Your app should now be running on localhost:7084