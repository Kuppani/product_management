const Joi = require('joi');
const SchemaModel = require('../config/schema');

const optionalParams = ['product_ids','subscription_ids'];
const sessionSchema = {
    hashKey: 'session_id',
    timestamps: true,
    schema: Joi.object({
        session_id: Joi.string(),
        product_ids: Joi.array(),
        subscription_ids: Joi.array()
    }).optionalKeys(optionalParams).unknown(true)
};

const attToGet = ['product_ids','subscription_ids','sessionId'];
const attToQuery = ['product_ids','subscription_ids','sessionId'];
const optionsObj = {
    attToGet,
    attToQuery,
};
const Session = SchemaModel(sessionSchema, optionsObj);

module.exports = Session;