const Joi = require('joi');
const SchemaModel = require('../config/schema');

const optionalParams = ['title', 'description','price', 'imageURL', 'retail_price', 'discount','collectionId'];
const productSchema = {
    hashKey: 'collectionType',
    rangeKey: 'collectionId',
    timestamps: true,
    schema: Joi.object({
        collectionType: 'product', //Product
        collectionId: Joi.string().alphanum(), //ProductId
        title: Joi.string(),
        description: Joi.string(),
        price: Joi.number().positive().min(1),
        imageURL: Joi.string(),
        retail_price: Joi.string(),
        discount:Joi.string()
       
    }).optionalKeys(optionalParams).unknown(true)
}


const attToGet = ['title', 'description','price', 'imageURL', 'retail_price', 'discount','collectionId'];
const attToQuery = ['title', 'description','price', 'imageURL', 'retail_price', 'discount','collectionId'];
const optionsObj = {
    attToGet,
    attToQuery,
};
const Product = SchemaModel(productSchema, optionsObj);

module.exports = Product;
